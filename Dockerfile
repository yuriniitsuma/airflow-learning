FROM --platform=amd64 apache/airflow:2.2.3

RUN python -m pip install --upgrade pip
RUN pip install 'apache-airflow[google_auth]'
RUN pip install 'apache-airflow[cncf.kubernetes]'

COPY plugins $AIRFLOW_HOME/plugins

RUN python -m pip check