# Airflow

> Yuri Niitsuma

## NGINX ingress

**config.yaml**

```yaml
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
networking:
  apiServerAddress: "0.0.0.0"
  apiServerPort: 7443
nodes:
  - role: control-plane
    kubeadmConfigPatches:
      - |
        kind: InitConfiguration
        nodeRegistration:
          kubeletExtraArgs:
            node-labels: "ingress-ready=true"
    extraPortMappings:
      - containerPort: 80
        hostPort: 80
        protocol: TCP
      - containerPort: 443
        hostPort: 443
        protocol: TCP
  - role: worker
  - role: worker
  - role: worker
```

```shell
kind create cluster --config config.yaml
```

Install Nginx ingress add-ons

```shell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
```

## Install monitoring stack

```shell
helm repo add grafana https://grafana.github.io/helm-charts
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

# Install loki
helm upgrade --install loki grafana/loki-stack \
  --version v2.5.0 \
  --create-namespace --namespace=monitoring \
  --set fluent-bit.enabled=true,promtail.enabled=false,loki.persistence.enabled=true,loki.persistence.storageClassName=standard,loki.persistence.size=5Gi

# Install prometheus
helm upgrade --install \
    --version v19.2.2 \
    --create-namespace \
    --namespace monitoring \
    prometheus prometheus-community/kube-prometheus-stack \
    --set grafana.enabled=false \
    -f kubernetes/prometheus_values.yaml

# Install Grafana
helm upgrade --install \
    --version v6.17.3 \
    --create-namespace \
    --namespace monitoring \
    grafana grafana/grafana

# get admin's password of grafana
kubectl get secret --namespace monitoring grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

# Create Ingress
bash -c "kubectl create ingress grafana -n monitoring --rule=grafana.localhost/\*=grafana:80"
```

Login on [Grafana](http://grafana.localhost) and add datasources:

- Loki: `http://loki:3100`
- Prometheus: `http://prometheus-kube-prometheus-prometheus:9090`

## External postgres.

Simulate Postgresql outside of airflow stack.

```shell
docker run --name airflow-postgres -e POSTGRES_PASSWORD=postgres -p 5432:5432 --network kind -d postgres:latest
```

## Airflow

```shell
helm repo add kedacore https://kedacore.github.io/charts

helm repo update

kubectl create namespace keda

helm install keda kedacore/keda \
    --namespace keda \
    --version "v2.0.0" \
    --debug
```

## Airflow secret key

```shell
kubectl create namespace airflow --dry-run=client -o yaml | kubectl apply -f -
kubectl create secret generic airflow-secret -n airflow --from-literal="webserver-secret-key=$(python3 -c 'import secrets; print(secrets.token_hex(16))')"
helm upgrade --install airflow --debug --create-namespace apache-airflow/airflow --namespace airflow --version v1.3.0 -f kubernetes/airflow_values.yaml
```

## Airflow Monitoring

With the following configuration, you can monitor the Airflow environment in Grafana.

```
airflow_dag_processing_import_errors
airflow_dag_processing_total_parse_time
airflow_dagbag_size
airflow_executor_open_slots
airflow_executor_queued_tasks
airflow_executor_running_tasks
airflow_pool_open_slots
airflow_pool_queued_slots_default_pool
airflow_pool_running_slots_default_pool
airflow_scheduler_critical_section_duration
airflow_scheduler_critical_section_duration_count
airflow_scheduler_critical_section_duration_sum
airflow_scheduler_heartbeat
airflow_scheduler_orphaned_tasks_adopted
airflow_scheduler_orphaned_tasks_cleared
airflow_schedulerjob_end
airflow_schedulerjob_start
airflow_triggererjob_end
airflow_triggererjob_start
airflow_triggers_running
```

## Object Storage

```shell
helm repo add minio https://charts.min.io/
helm repo update
bash -c "helm upgrade --install --namespace minio --create-namespace \
    --set rootUser=rootuser,rootPassword=rootpass123,replicas=4,resources.requests.memory=2Gi,buckets[0].name=airflow-logs \
    minio minio/minio"
bash -c "k create ingress minio -n minio --rule=minio.localhost/\*=minio-console:9001"
```

Go to minio console [http://minio.localhost](http://minio.localhost) with `rootuser` and `rootpass123` and create a bucket `airflow-logs`.

Create user with `readwrite` policy:

- Access Key: `testtest`
- Secret Key: `testtest`

## Logs

Create a S3 connection (Admin -> Connections -> Add new connection) on airflow with these parameters:

**conn_id**: `AirflowS3Logs`
**conn_type**: `S3`

**Extra**:

```json
{
  "aws_access_key_id": "testtest",
  "aws_secret_access_key": "testtest",
  "host": "http://minio.minio.svc.cluster.local:9000"
}
```
