from airflow.models import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.utils.task_group import TaskGroup

from datetime import datetime

default_args = {
    'start_date': datetime(2020, 1, 1),
}

with DAG(
    dag_id='parallel_dag',
    default_args=default_args,
    catchup=False,
) as dag:
    task_1 = BashOperator(
        task_id='task_1',
        bash_command='sleep 3',
        do_xcom_push=False
    )

    with TaskGroup('processing_tasks') as processing_tasks:
        with TaskGroup('spark_tasks') as spark_tasks:
            subdag_task = BashOperator(
                task_id='task_3',
                bash_command='sleep 10',
                do_xcom_push=False
            )
        with TaskGroup('flink_tasks') as flink_tasks:
            subdag_task = BashOperator(
                task_id='task_3',
                bash_command='sleep 10',
                do_xcom_push=False
            )
        with TaskGroup('druid_tasks') as druid_tasks:
            subdag_task = BashOperator(
                task_id='task_3',
                bash_command='sleep 10',
                do_xcom_push=False
            )
        with TaskGroup('kafka_consumer_tasks') as kafka_consumer_tasks:
            subdag_task = BashOperator(
                task_id='task_3',
                bash_command='sleep 10',
                do_xcom_push=False
            )
        with TaskGroup('debezium_tasks') as debezium_tasks:
            subdag_task = BashOperator(
                task_id='task_3',
                bash_command='sleep 10',
                do_xcom_push=False
            )
        with TaskGroup('rforest_training') as rforest_training:
            import_data = BashOperator(
                task_id='s3-dist-cp_training_data',
                bash_command='sleep 10',
                do_xcom_push=False
            )
            training = BashOperator(
                task_id='training_model',
                bash_command='sleep 10',
                do_xcom_push=False
            )
            export_model = BashOperator(
                task_id='export_model',
                bash_command='sleep 10',
                do_xcom_push=False
            )
            import_data >> training >> export_model

    task_4 = BashOperator(
        task_id='task_4',
        bash_command='sleep 3',
        do_xcom_push=False
    )

    # task_1 >> task_2 >> task_4
    # task_1 >> task_3 >> task_4
    task_1 >> processing_tasks >> task_4
