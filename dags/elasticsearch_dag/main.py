from airflow.operators.python import PythonOperator
from airflow import DAG

from elasticsearch_plugin.hooks.elastic_hook import ElasticHook
from elasticsearch_plugin.operators.postgres_to_elastic import PostgresToElasticOperator

from datetime import datetime

default_args = {
    'start_date': datetime(2020, 1, 1),
}


# create a function _print_es_info to hook elastichook


def _print_es_info():
    hook = ElasticHook()
    print(hook.info())


with DAG('elasticsearch_dag',
         default_args=default_args,
         schedule_interval='@daily',
         catchup=False
         ) as dag:
    print_es_info = PythonOperator(
        task_id='print_es_info',
        python_callable=_print_es_info,
    )

    connections_to_es = PostgresToElasticOperator(
        task_id='connections_to_es',
        sql="SELECT * FROM connection",
        index="connections",
    )

    print_es_info >> connections_to_es
