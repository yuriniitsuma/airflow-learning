from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from airflow.models import Variable

default_args = {
    'owner': 'Yuri Niitsuma',
    'depends_on_past': False,
    'start_date': days_ago(0),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 2,
}

resource_config = {"KubernetesExecutor": {"request_memory": "200Mi",
                                          "limit_memory": "200Mi",
                                          "request_cpu": "200m",
                                          "limit_cpu": "200m"}}


def _execute():
    print("Eu to aqui no Kubernetes")


with DAG(dag_id='kubernetes_sample', schedule_interval=None,
         tags=['analytics'], default_args=default_args) as dag:

    task = PythonOperator(
        task_id='kubernetes_sample',
        python_callable=_execute,
        start_date=days_ago(0),
        owner='airflow',
        queue='kubernetes',
        executor_config=resource_config
    )

    task
